from joblib import dump, load
import pandas as pd
import numpy as np
def ask_clean_input(questions, num_rep):
    lst_ans = [];
    for i in range(0, len(questions)):
        opt = str(num_rep[i].keys()).split("(")[1]
        ans = input(questions[i]+" "+opt+": ")
        lst_ans.append(num_rep[i][ans])
    return lst_ans;

clf = load('forest_classifier.joblib')
lst_representation = [{'nan': 0, 'Yes': 1, 'No': 2}, {'No': 0, 'Maybe/Not sure': 1, 'Yes, I experienced': 2, 'Yes, I observed': 3}, {'1.0': 0, '2.0': 1, '3.0': 2, '4.0': 3, '5.0': 4}, {'18.0': 0, '20.0': 1, '21.0': 2, '22.0': 3, '23.0': 4, '24.0': 5, '25.0': 6, '26.0': 7, '27.0': 8, '28.0': 9, '29.0': 10, '30.0': 11, '31.0': 12, '32.0': 13, '33.0': 14, '34.0': 15, '35.0': 16, '36.0': 17, '37.0': 18, '38.0': 19, '39.0': 20, '40.0': 21, '41.0': 22, '42.0': 23, '43.0': 24, '44.0': 25, '45.0': 26, '46.0': 27, '47.0': 28, '48.0': 29, '49.0': 30, '50.0': 31, '51.0': 32, '52.0': 33, '53.0': 34, '54.0': 35, '55.0': 36, '56.0': 37, '57.0': 38, '58.0': 39, '59.0': 40, '60.0': 41, '61.0': 42, '62.0': 43, '64.0': 44, '66.0': 45}, {'Not applicable to me': 0, 'Often': 1, 'Never': 2, 'Sometimes': 3, 'Rarely': 4}, {'Not applicable to me': 0, 'Often': 1, 'Never': 2, 'Sometimes': 3, 'Rarely': 4}, {'Very easy': 0, 'Neither easy nor difficult': 1, 'Somewhat difficult': 2, 'Somewhat easy': 3, "I don't know": 4, 'Difficult': 5}, {"I don't know": 0, 'Yes': 1, 'No': 2}, {'Physical health': 0, 'Same level of comfort for each': 1, 'Mental health': 2}, {'Maybe': 0, 'Yes': 1, 'No': 2}, {'0.0': 0, '1.0': 1}]
self_lst = ["Do you know the options for mental health care available under your employer-provided health coverage?","Have you observed or experienced an unsupportive or badly handled response to a mental health issue in your current or previous workplace?" ,"Overall, how well do you think the tech industry supports employees with mental health issues?", "What is your age?", "If you have a mental health disorder, how often do you feel that it interferes with your work when NOT being treated effectively (i.e., when you are experiencing symptoms)?", "If you have a mental health disorder, how often do you feel that it interferes with your work when being treated effectively?","If a mental health issue prompted you to request a medical leave from work, how easy or difficult would it be to ask for that leave?", "Is your anonymity protected if you choose to take advantage of mental health or substance abuse treatment resources provided by your employer?"]
action_lst = ["Would you feel more comfortable talking to your coworkers about your physical health or your mental health?", "Would you feel comfortable discussing a mental health issue with your direct supervisor(s)?", "Are you openly identified at work as a person with a mental health issue?"]
lst_ans = ask_clean_input(self_lst, lst_representation);
prediction = clf.predict([lst_ans]);
print("Open identify for user is: "+prediction)

#TODO fix the age range instead of sequence use original value in int

