from collections import Counter
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import ComplementNB
from collections import Counter
from sklearn.ensemble import AdaBoostClassifier
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from joblib import dump, load

def split_train(feature, label, cutoff, clf):
    feature = feature.sample(frac=1).reset_index(drop=True)
    label = label.sample(frac=1).reset_index(drop=True)#shuffle because need randomness
    cut= round(len(label)*cutoff)
    train_X, train_y = feature.iloc[:cut], label.iloc[:cut]
    test_X, test_y = feature.iloc[cut:], label.iloc[cut:]
    clf.fit(train_X, train_y)
    return clf.score(test_X, test_y)

def split_feature_label(n_df, clf):
    feature_set = n_df[n_df.columns[:-3]].copy(deep=True)#assumption, last three columns are label to predict
    label_set = n_df[n_df.columns[-3:]].copy(deep=True)
    
    print("discussion coworker")
    label = label_set[label_set.columns[0]]#predict how comfortabel to predict discussion with coworker
    total=0.0;
    for i in range(0, 10):
        score = split_train(feature_set, label, .7, clf);
        total+=score
    print(total/10);
    
    print("dicusssion supervisor")
    label = label_set[label_set.columns[1]]#predict how comfortabel to predict discussion with supervisor
    total=0.0;
    for i in range(0, 10):
        score = split_train(feature_set, label, .7, clf);
        total+=score
    print(total/10);

    print("open identify")
    label = label_set[label_set.columns[2]]#predict how comfortabel to offically let others know
    total=0.0;
    for i in range(0, 20):
        score = split_train(feature_set, label, .7, clf);
        total+=score
    print(total/20);

def run_variety_algorithm(df):
    clf = MultinomialNB()
    print("Naive Bayes")
    split_feature_label(df, clf)
    print()

    clf = KNeighborsClassifier(algorithm="kd_tree", n_neighbors=9)
    print("k nearest neighbor classifier")
    split_feature_label(df, clf)
    print()

    clf = SVC(kernel='linear')
    print("SVM classifier")
    split_feature_label(df, clf)
    print()

    clf = AdaBoostClassifier(n_estimators=40, learning_rate=.1)
    print("AdaBoost")
    split_feature_label(df, clf)
    print()

    clf = RandomForestClassifier(n_estimators=50, max_depth=None, random_state=0)
    print("Random Forest Classifier")
    split_feature_label(df, clf)
    print()
    
    return clf;#only returns the last model to show predicting capability


print("predicting based on normalized data")
df = pd.read_csv("openness_normalized.csv").drop("Unnamed: 0", axis = 1)
run_variety_algorithm(df);

print("predicting based on one hot data")
df_feature = pd.read_csv("openness_onehot_feature.csv")
df_label = pd.read_csv("openness_onehot_label.csv")
df = df_feature.merge(df_label).drop("Unnamed: 0", axis = 1)
run_variety_algorithm(df);

print("predicting based on cleaned data with no addition steps")
df = pd.read_csv("openness_no_onehot.csv").drop("Unnamed: 0", axis = 1)
clf_example = run_variety_algorithm(df);

#writes last model to output
dump(clf_example, 'forest_classifier.joblib') 
