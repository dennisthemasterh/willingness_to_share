import pandas as pd
import numpy as np
import math

def clean_raw_df(filename):
    df = pd.read_csv(filename)
    #col_drop_lst is dropped cause of lack of response
    col_drop_lst=[0, 1, 2, 14, 17, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 40, 43, 45, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 97, 99, 101, 102, 105, 107, 109, 110, 111, 113, 115, 116, 117, 119, 120, 121, 122];
    df = df.drop(df.columns[col_drop_lst], axis=1)
    #dropping all non tech related employees
    lst_drop = []
    for index in df.index:
        tech_1 = df.iloc[index].loc["Is your employer primarily a tech company/organization?"]
        tech_2 = df.iloc[index].loc["Is your primary role within your company related to tech/IT?"]
        tech_3 = df.iloc[index].loc["Was your employer primarily a tech company/organization?"]
        if(str(tech_1)=="nan" or str(tech_2)=="nan"):
            lst_drop.append(index)

    df = df.drop(df.index[lst_drop])
    return df;

def replace_value_w_integer(df):
    col_rep_key_lst=[]#keeps track of column and the dictionary used to decipher the number representation
    for i in range(0, len(df.columns)):
        col = df.columns[i]
        column = df[col]
        option = list(set(column.values))#gets unique values
        rep_key = {}#allow decypher of number representation and value
        for i in range(0, len(option)):
            rep_key[option[i]] = i
        col_rep_key_lst.append(rep_key)
    # print(col_rep_key_lst)

    #purpose is to replace all the values with numeric representation
    for col in range(0, len(df.columns)):
        col_lst = df[df.columns[col]].copy(deep=True)
        rep_key = col_rep_key_lst[col]
        for index in range(0, len(col_lst)):
            val = col_lst.iloc[index]
            num_rep = rep_key[val]#gets the correct number representation for the value  
            col_lst.iloc[index] = int(num_rep)#find why some store value as float
        df[df.columns[col]]=col_lst
    return df;

def clean_openess_no_one_hot(df, self_lst, action_lst, feature, label):
    #get rid of all non tech people
    lst_rem = []
    for index in range(0, len(df)):
        tech_1 = df.iloc[index].loc["Is your employer primarily a tech company/organization?"]
        tech_2 = df.iloc[index].loc["Is your primary role within your company related to tech/IT?"]
        if(tech_1==0 or tech_2==0):#remove all nontech particapants
            lst_rem.append(index);
    
    #combine into new dataframe with only the data needed
    new_df = df.filter(self_lst+action_lst, axis=1)
    new_df.set_axis(feature+label, axis=1, inplace=True)
    return replace_value_w_integer(new_df);

def clean_openess_one_hot(df, self_lst, action_lst, prefixes):
    #get rid of all non tech people
    lst_rem = []
    for index in range(0, len(df)):
        tech_1 = df.iloc[index].loc["Is your employer primarily a tech company/organization?"]
        tech_2 = df.iloc[index].loc["Is your primary role within your company related to tech/IT?"]
        if(tech_1==0 or tech_2==0):#remove all nontech particapants
            lst_rem.append(index);

    new_df = df.filter(self_lst+action_lst, axis=1)
    df1 = new_df.copy(deep=True)#gets a new copy of the dataframe for feature generation

    label = new_df[new_df.columns[-3:]].copy(deep=True)#divides the label and features
    feature = df1.drop(df.columns[[6, 7, 8]], axis=1)
    feature_set = pd.DataFrame(dtype=np.int8)

    #turn feature into feature set
    for index in range(0, 6):
        target = df1[df1.columns[index]]
        dummy_features = pd.get_dummies(list(target), dummy_na=True, prefix=prefixes[index])
        feature_set = pd.concat([feature_set, dummy_features], axis=1)

    label_set = label.copy(deep=True)#gets a prediction set for classification
    return feature_set, replace_value_w_integer(label_set);

def normalized_data(new_df):
    for col in range(0, 8):
        lst = new_df[new_df.columns[col]];
        s = list(set(lst.values))#gets unique values
        if(math.isnan(s[0])):#special code to remove nan, nan is smallest value
            s=s[1:]
        
        for i in range(0, len(lst)):
            l_min = min(s)
            l_max = max(s)
            lst.iloc[i]=((lst.iloc[i]-l_min)/(l_max-l_min))
    return new_df;

df = clean_raw_df("OSMI Mental Health in Tech Survey 2017.csv");
#column is an employee's perception of employee and mental illness
self_lst = ["Do you know the options for mental health care available under your employer-provided health coverage?","<strong>Have you observed or experienced an unsupportive or badly handled response to a mental health issue in your current or previous workplace?</strong>" ,"Overall, how well do you think the tech industry supports employees with mental health issues?", "What is your age?", "If you have a mental health disorder, how often do you feel that it interferes with your work <strong>when <em>NOT</em> being treated effectively (i.e., when you are experiencing symptoms)?</strong>", "If you have a mental health disorder, how often do you feel that it interferes with your work <strong>when being treated effectively?</strong>","If a mental health issue prompted you to request a medical leave from work, how easy or difficult would it be to ask for that leave?", "Is your anonymity protected if you choose to take advantage of mental health or substance abuse treatment resources provided by your employer?"]
feature = ["know_option","observed_bad",  "tech_support", "age", "interfer_work_not_treated", "interfer_work_treated", "easy_leave", "anonymity"]

#column is whether employee is willing discuss
action_lst = ["Would you feel more comfortable talking to your coworkers about your physical health or your mental health?", "Would you feel comfortable discussing a mental health issue with your direct supervisor(s)?", "Are you openly identified at work as a person with a mental health issue?"]
label = ["discussion coworker", "dicusssion supervisor", "open identify"]

df_clean_no_onehot = clean_openess_no_one_hot(df, self_lst, action_lst, feature, label)
df_clean_onehot_feature, df_clean_onehot_label = clean_openess_one_hot(df, self_lst, action_lst, feature)
df_clean_normalized = normalized_data(df_clean_no_onehot)

df_clean_no_onehot.to_csv("openness_no_onehot.csv")
df_clean_onehot_feature.to_csv("openness_onehot_feature.csv")
df_clean_onehot_label.to_csv("openness_onehot_label.csv")
df_clean_normalized.to_csv("openness_normalized.csv")
